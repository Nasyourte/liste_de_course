<?php

// connect à la db
$host = 'localhost';
$db   = 'keanu';
$user = 'bob';
$pass = 'toto'; // ne pas faire ça dans la vrai vie !

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);


function getProduits(){
    global $pdo;
    $req = $pdo->query('select * from course;');
    return $req->fetchAll();
}

function addProduit($nom, $qt){
    // ajouter le produit $nom avec $qt
    // à la liste $produits
    global $pdo;
    $req = $pdo->prepare("insert into course (nom, qt) values (?, ?);");
    $req->execute([$nom, $qt]);
    
}

function delete($id){
    global $pdo;
    $req = $pdo->prepare("delete from course where id=?;");
    $req->execute([$id]);
}