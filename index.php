<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste course</title>
</head>
<body>
<h1>Liste de courses</h1>
<div class="list">
 
   <header class="flex">
    <div class="prod">PRODUIT</div>
    <div class="qt">QT</div>
    <div class="btn">BTN</div>
  </header>
 <?php
    include 'model.php';
    foreach( getProduits() as $produit){
 ?>
   <div class="flex">
    <div class="prod"><?= $produit['nom'] ?></div>
    <div class="qt"><?= $produit['qt'] ?></div>
    <div class="btn">


      <a href="supprimer.php?id=<?= $produit['id'] ?>">Suppr</a>


    </div>
  </div>

 <?php } ?>
  
  <!-- formulaire -->
  <form action="ajouter.php" method="post">
    <div class="flex">
      <div class="prod">
        <input type="text" name="nom">
      </div>
      <div class="qt">
        <input type="number" name="qt">
      </div>
      <div class="btn">
        <input type="submit" value="Add">
      </div>
    </div>
  </form>
  
</div>

<style>
    .list{
  max-width: 512px;
  display: flex;
  flex-direction: column;
  justify-content: center;
}
.flex{
  display: flex;
  width: 100%;
  justify-content: space-around;
}
.qt, .btn{flex: 1;}
.prod{flex: 3;}
input{width: 80%}
header{font-weight: bold;}
</style>
</body>
</html>